import "../style/Blog.css";
import {useEffect} from "react";

function Blog(props) {

    useEffect(() => {
        document.title = 'Blog | TAKAHIRO YAMADA WEB CREATION';
    }, []);


    return (
        <div className="sub_container">
            <h2>Blog</h2>
            <section>
                <div>
                    <h3>TilteTilteTilteTiltlteTilteTilteTilteTilteTilteTilte</h3>
                    <p>texttexttext</p>
                    <span>Tag001</span> <span>Tag001</span> <span>Tag001</span> <span>Tag001</span>
                    <p>xx-xx-20xx</p>
                </div>
                <div>
                    <h3>TilteTilteTilteTiltlteTilteTilteTilteTilteTilteTilte</h3>
                    <p>texttexttext</p>
                    <span>Tag001</span> <span>Tag001</span> <span>Tag001</span> <span>Tag001</span>
                    <p>xx-xx-20xx</p>
                </div>
                <div>
                    <h3>TilteTilteTilteTiltlteTilteTilteTilteTilteTilteTilte</h3>
                    <p>texttexttext</p>
                    <span>Tag001</span> <span>Tag001</span> <span>Tag001</span> <span>Tag001</span>
                    <p>xx-xx-20xx</p>
                </div>
                <div>
                    <h3>TilteTilteTilteTiltlteTilteTilteTilteTilteTilteTilte</h3>
                    <p>texttexttext</p>
                    <span>Tag001</span> <span>Tag001</span> <span>Tag001</span> <span>Tag001</span>
                    <p>xx-xx-20xx</p>
                </div>
                <div>
                    <h3>TilteTilteTilteTiltlteTilteTilteTilteTilteTilteTilte</h3>
                    <p>texttexttext</p>
                    <span>Tag001</span> <span>Tag001</span> <span>Tag001</span> <span>Tag001</span>
                    <p>xx-xx-20xx</p>
                </div>
                <div>
                    <h3>TilteTilteTilteTiltlteTilteTilteTilteTilteTilteTilte</h3>
                    <p>texttexttext</p>
                    <span>Tag001</span> <span>Tag001</span> <span>Tag001</span> <span>Tag001</span>
                    <p>xx-xx-20xx</p>
                </div>
                <div>
                    <h3>TilteTilteTilteTiltlteTilteTilteTilteTilteTilteTilte</h3>
                    <p>texttexttext</p>
                    <span>Tag001</span> <span>Tag001</span> <span>Tag001</span> <span>Tag001</span>
                    <p>xx-xx-20xx</p>
                </div>
                <div>
                    <h3>TilteTilteTilteTiltlteTilteTilteTilteTilteTilteTilte</h3>
                    <p>texttexttext</p>
                    <span>Tag001</span> <span>Tag001</span> <span>Tag001</span> <span>Tag001</span>
                    <p>xx-xx-20xx</p>
                </div>
                <div>
                    <h3>TilteTilteTilteTiltlteTilteTilteTilteTilteTilteTilte</h3>
                    <p>texttexttext</p>
                    <span>Tag001</span> <span>Tag001</span> <span>Tag001</span> <span>Tag001</span>
                    <p>xx-xx-20xx</p>
                </div>
                <div>
                    <h3>TilteTilteTilteTiltlteTilteTilteTilteTilteTilteTilte</h3>
                    <p>texttexttext</p>
                    <span>Tag001</span> <span>Tag001</span> <span>Tag001</span> <span>Tag001</span>
                    <p>xx-xx-20xx</p>
                </div>


            </section>
        </div>
    );
}

export default Blog;
import "../style/Contact.css"
import React, {useEffect, useRef, useState} from "react";
import emailjs from '@emailjs/browser';
import {useForm} from "react-hook-form";
import copy from "../asset/copy.png"

function Contact(props) {
    useEffect(() => {
        document.title = 'Contact | TAKAHIRO YAMADA WEB CREATION';
    }, []);
    emailjs.init("hupchnZbg9mZaDmHZ");

    const [copyTocb, setCopyTocb] = useState(false);
    const [formindic, setFormindic] = useState("");
    const [formindex, setFormindex] = useState(0);
    const [firstname, setFirstName] = useState('');
    const [lastname, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');
    const [emailcheck, setEmailcheck] = useState('');
    let [submit, setSubmit] = useState(false);
    const {
        register, handleSubmit, formState: {errors},
    } = useForm({
        mode: 'onBlur', reValidateMode: 'onBlur', criteriaMode: 'all',
    });
    const form = useRef();

    useEffect(() => {
        if (firstname && lastname && !emailcheck && message) {
            setSubmit(true)
        } else {
            setSubmit(false)
        }
    });
    useEffect(() => {
        setFormindic(-100 * formindex + "%")
    },);

    const copytocilpbord = (event) => {
        navigator.clipboard.writeText("takahirodev06@gmail.com");
        setCopyTocb(true)
    };

    const handleFirstNameChange = (event) => {
        setFirstName(event.target.value);
        console.log(event.target.value);
    };
    const handleLastNameChange = (event) => {
        setLastName(event.target.value);
    };

    const handleEmailChange = (event) => {
        const pattern = /^[A-Za-z0-9]{1}[A-Za-z0-9_.-]*@{1}[A-Za-z0-9_.-]+.[A-Za-z0-9]+$/;
        if (pattern.test(event.target.value)) {
            setEmailcheck("")
        } else {
            setEmailcheck("invalide")
        }
        setEmail(event.target.value);
    };
    const handleMessageChange = (event) => {
        setMessage(event.target.value);
    };
    const addFormIndic = (event) => {
        return setFormindex(formindex + 1)
    };
    const removeFormIndic = (event) => {
        return setFormindex(formindex - 1)
    }
    const onSubmit = () => {
        emailjs.sendForm('service_1mo2u0e', 'template_ibm70v8', form.current)
            .then(function (response) {
                window.alert("Your message was sent successfully.");
                addFormIndic()
            }, function (error) {
                window.alert("A transmission error has occurred.");
                console.log('FAILED...', error);
            });
    };

    return (
        <div className="sub_container">
            <h2>Contact</h2>
            <section className="contact_address">
                <p>Please contact us via this email.</p>
                <h3>takahirodev06@gmail.com
                    {copyTocb ? <img src={copy} alt="copy" onClick={copytocilpbord} className={"filtegreen"}/>
                        : <img src={copy} alt="copy" onClick={copytocilpbord} className={"filterwhite"}/>
                    }       </h3>
            </section>
            <section className="contact_form">
                <form onSubmit={handleSubmit(onSubmit)} method="post" ref={form}>

                    <div className="form_indicate">
                        {formindex === 0 ? <p className="form_indicate_curr"><span></span></p> :
                            <p><span></span></p>}
                        {formindex === 1 ? <p className="form_indicate_curr"><span></span></p> :
                            <p><span></span></p>}
                        {formindex === 2 ? <p className="form_indicate_curr"><span></span></p> :
                            <p><span></span></p>}
                        {formindex === 3 ? <p className="form_indicate_curr"><span></span></p> : <p><span></span></p>}
                    </div>
                    <div className={"form_steps"}>
                        <div className={"form_step"} style={{marginLeft: formindic}}>
                            <input type="text" placeholder="firstname"
                                   value={firstname} {...register('firstname', {required: true})}
                                   onChange={handleFirstNameChange}/>
                            {errors.firstname && <p className="form_alert">* required field</p>}
                            <input type="text" placeholder="lastname"
                                   value={lastname}{...register('lastname', {required: true})}
                                   onChange={handleLastNameChange}/>
                            {errors.lastname && <p className="form_alert">* required field</p>}
                        </div>
                        <div className={"form_step"}>
                            <input type="email" placeholder="your_email@address.com" className="width100"
                                   value={email}   {...register('email', {required: true})}
                                   onChange={handleEmailChange} tabindex="-1"/>
                            {errors.email && <p className="form_alert">* required field</p>}
                            {!errors.email && emailcheck &&
                                <p className="form_alert">* address email is not validated</p>}

                        </div>
                        <div className={"form_step"}>
                            <textarea placeholder="Message" className={"width100"}
                                      value={message} {...register('message', {required: true})}
                                      onChange={handleMessageChange} tabindex="-1"/>
                            {errors.message && <p className="form_alert">* required field</p>}
                        </div>
                        <div className={"form_step"}>
                            <p>Your message was sent successfully.</p>
                        </div>
                    </div>

                    <div className={"form_switch"}>
                        {formindex > 0 ? <button onClick={removeFormIndic} tabindex="-1">Back</button> :
                            <div></div>}
                        {formindex < 2 ? <button onClick={addFormIndic} tabindex="-1">Next</button> : null}
                        {formindex === 2 ? <input type="submit" value="Send" disabled={!submit}/> : null}
                    </div>

                </form>
            </section>

        </div>
    );
}

export default Contact;
import * as PropTypes from "prop-types";
import "../style/About.css"
import React, {useEffect} from "react";
import portait from "../asset/35783.jpg"

function About(props) {

    useEffect(() => {
        document.title = 'About | TAKAHIRO YAMADA WEB CREATION';
    }, []);

    return (
        <div className="sub_container">
            <h2>About</h2>
            <section>
                <img src={portait} alt="" style={{width: "50%"}}/>
                <h3>山田 隆広</h3>
                <h3>Takahiro YAMADA</h3>
                <p>Front-end web developer</p>
            </section>
        </div>
    );
}

export default About;
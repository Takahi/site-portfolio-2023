import "../style/Services.css"
import Works from "./Works";
import {useEffect} from "react";
function Services(props) {

    useEffect(() => {
        document.title = 'Services | TAKAHIRO YAMADA WEB CREATION';
    }, []);


    return (
        <div className="sub_container">
            <h2>Services</h2>
        </div>
    );
}

export default Services;
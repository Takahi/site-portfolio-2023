import "../style/TopPage.css"
import {Link} from "react-router-dom";
import * as PropTypes from "prop-types";
import {useEffect, useState} from "react";

function Helmet(props) {
    return null;
}

function TopPage(props) {

    useEffect(() => {
        document.title = 'TAKAHIRO YAMADA WEB CREATION';
    }, []);


    const [offset, setOffset] = useState(0);

    useEffect(() => {
        const onScroll = () => setOffset(window.pageYOffset / 3);
        window.removeEventListener('scroll', onScroll);
        window.addEventListener('scroll', onScroll, {passive: true});
        return () => window.removeEventListener('scroll', onScroll);
    }, []);


    return (
        <div className="top_container">
            <section className="top_code" style={{top: offset + "px"}}>
                <p>&lt;!DOCTYPE html&gt;</p>
                <p>&lt;html&gt;</p>
                <p>&nbsp;&nbsp;&lt;head&gt;</p>
                <p>&nbsp;&nbsp;&nbsp; &lt;title&gt;My web page&lt;/title&gt;</p>
                <p>&nbsp;&nbsp;&lt;/head&gt;</p>
                <p>&nbsp;&nbsp;&lt;body&gt;</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;h1&gt;Hello, world!&lt;/h1&gt;</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;This is my first web page.&lt;/p&gt;</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;It contains a&lt;strong&gt;main heading&lt;/strong&gt;</p>
                <p>&nbsp;&nbsp;&lt;em&gt; paragraph &lt;/em&gt;.</p>
                <p>&nbsp;&nbsp;&lt;form&gt;</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;label for="firstname"&gt;First name: &lt;/label&gt;</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;input type="text" name="firstname" required&gt;</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;label for="lastname" &gt;Last name: &lt;/label&gt;</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;input type="text" name="lastname" required&gt;</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;label for="email"&gt;email: &lt;/label&gt;</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;input type="email" name="email" required&gt;</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;label for="password" &gt;password: &lt;/label&gt;</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;input type="password" name="password" required&gt;</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;input type="submit" value="Login!"&gt;</p>
                <p>&nbsp;&lt;/form&gt;</p>
                <p>&nbsp;&lt;/body&gt;</p>
                <p>&lt;/html&gt;</p>
            </section>

            <section className="top_view gridcenter">
                <h1 className="top_title Gruppo"> TAKAHIRO <br/> YAMADA <br/> WEB CREATION</h1>
                <h2 className="top_subt Gruppo">Front-end web developer</h2>
            </section>

            <section className="top_navi">
                <ul>
                    <Link to="/">
                        <li>Home</li>
                    </Link>
                    <Link to="/services">
                        <li>Services</li>
                    </Link>
                    <Link to="/works">
                        <li>Works</li>
                    </Link>
                    <Link to="/about">
                        <li>About</li>
                    </Link>
                    <Link to="/blog">
                        <li>Blog</li>
                    </Link>
                    <Link to="/contact">
                        <li>Contact</li>
                    </Link>
                </ul>
            </section>
        </div>
    );
}

export default TopPage;